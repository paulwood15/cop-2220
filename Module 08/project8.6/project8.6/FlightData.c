#include "FlightData.h"
#include <stdlib.h>

void printFlightData(FlightData fd) 
{
	printf("%s\t%s\t%s\t%d\n", fd.flight_num, fd.origin_code, fd.dest_code, fd.timestamp);
}


int compare_DESTCODE(const void *ptr1, const void *ptr2)
{
	return strcmp(((FlightData*)(ptr1))->dest_code, ((FlightData*)(ptr2))->dest_code);
}



//FlightDataNode* initList()
//{
//	FlightDataNode* root = malloc(sizeof(FlightDataNode));
//	root->prev_ptr = malloc(sizeof(FlightDataNode));
//	root->next_ptr = malloc(sizeof(FlightDataNode));
//	root->cur_data = malloc(sizeof(FlightData));
//
//	return root;
//}
//
//int insertFlightData(FlightDataNode *root_ptr, FlightData *fd, int (*insert_f)(void *ptr1, void *ptr2))
//{
//	FlightDataNode *query_node_ptr = root_ptr;
//	FlightDataNode *new_node = malloc(sizeof(FlightDataNode));
//	new_node->cur_data = fd;
//
//	do {
//		char *cur_cmp = malloc(AIRPORT_CODE * sizeof(char));
//		strncpy_s(cur_cmp, AIRPORT_CODE, query_node_ptr->cur_data->dest_code, AIRPORT_CODE + 1);
//
//		printf("comparing: %s and %s\n", cur_cmp, fd->dest_code);
//		if (insert_f((void*)cur_cmp, (void*)fd->dest_code) == 0) {
//			FlightDataNode *temp = query_node_ptr->next_ptr;
//			query_node_ptr->next_ptr = new_node;
//			new_node->prev_ptr = query_node_ptr;
//			new_node->next_ptr = temp;
//			break;
//		}
//		else if (insert_f((void*)cur_cmp, (void*)fd->dest_code) < 0) {
//			FlightDataNode *temp = query_node_ptr->prev_ptr;
//			query_node_ptr->prev_ptr = new_node;
//			temp->next_ptr = new_node;
//			new_node->prev_ptr = temp;
//			break;
//		}
//		//greater than
//		else {
//			//continue
//		}
//
//		query_node_ptr = query_node_ptr->next_ptr;
//		free(cur_cmp);
//	} while (query_node_ptr != NULL);
//}
//
//int removeFlightData_index(FlightDataNode *root_ptr, int index)
//{
//	return 0;
//}
//
//int removeFlightData(FlightDataNode *root_ptr, FlightData *rm_data)
//{
//	return 0;
//}
