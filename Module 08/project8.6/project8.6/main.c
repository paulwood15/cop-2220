//Paul Wood
//04/08/2018
//Project 8.6 - creating and sorting arrays of structs



#include <stdio.h>
#include <stdlib.h>
#include "FlightData.h"
#include <time.h>


#define A_SIZE 3000


int main(void) {

	FILE *flight_data;
	FILE *sorted_data;
	time_t posix

	FlightData flight_records[A_SIZE] = {NULL};
	char flight_records_readable_time[A_SIZE][TIME_SIZE] = { { NULL }, { NULL} };

	if (fopen_s(&flight_data, "./acars.bin", "rb") == 0) {
		//File opened successfully
	}
	else {
		printf("Error reading file!\n");
		system("pause");
		return -1;
	}

	if (fopen_s(&sorted_data, "./sorted_data.txt", "w") == 0) {
		//File opened successfully
	}
	else {
		printf("Error reading file!\n");
		system("pause");
		return -1;
	}


	/*printf("\t  FLIGHT DATA\n\n");
	printf("flight\torigin\tdest\ttimestamp\n");
	printf("---------------------------------\n");*/

	int i = 0;
	FlightData data_read;
	while (fread_s(&data_read, sizeof(FlightData), sizeof(FlightData), 1, flight_data) != 0) {
		//read data to array
		flight_records[i] = data_read;

		//convert POSIX to readable
		posix = data_read.timestamp;
		struct tm _tm;
		gmtime_s(&_tm, &posix);
		asctime_s(flight_records_readable_time[i], TIME_SIZE, &_tm);
		
		//print flight data
		//printFlightData(flight_records[i]);
		i++;
	}

	//printf("%d\n", i);

	//sort records by destination code
	qsort(flight_records, i, sizeof(FlightData), compare_DESTCODE);
	                                                                                    
	//print info to txt file
	for (int j = 0; j < i; j++) {
		//printf("%s\t%s\t%s\t%d\n", flight_records[j].flight_num, flight_records[j].origin_code, flight_records[j].dest_code, flight_records_readable_time[j]);
		fprintf(sorted_data, "%s\t%s\t%s\t%s\n", flight_records[j].flight_num, flight_records[j].origin_code, flight_records[j].dest_code, flight_records_readable_time[j]);
	}


	//counts each individual dest code
	int j = 0;
	//int sum_check = 0;
	while (j < i) {
		char code[AIRPORT_CODE];
		strcpy_s(code, AIRPORT_CODE, flight_records[j].dest_code);
		int count = 1;
		while ((j + count < A_SIZE) && (strcmp(code, flight_records[j + count].dest_code) == 0)) {
			count++;
		}
		printf("Num of \"%s\" is %d\n", code, count);
		//sum_check += count;

		j += count;
	}

	//printf("records read: %d\ntotal counted: %d\n", i, sum_check);

	fclose(flight_data);
	fclose(sorted_data);
	system("pause");

	return 0;
}
