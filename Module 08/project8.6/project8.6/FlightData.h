#pragma once
#include <stdio.h>
#include <string.h>

#ifndef FLIGHTDATA_H
#define FLIGHTDATA_H

#define FLIGHT_NUM 7
#define AIRPORT_CODE 5
#define TIME_SIZE 30

typedef struct FlightDataNode FlightDataNode;

typedef struct FlightData_struct {
	char flight_num[FLIGHT_NUM];
	char origin_code[AIRPORT_CODE];
	char dest_code[AIRPORT_CODE];
	int timestamp;
}FlightData;

void printFlightData(FlightData fd);
int compare_DESTCODE(const void* ptr1, const void* ptr2);


//struct FlightDataNode {
//	FlightDataNode *prev_ptr;
//	FlightDataNode *next_ptr;
//	FlightData *cur_data;
//};
//
//FlightDataNode* initList();
//int insertFlightData(FlightDataNode *root_ptr, FlightData* fd, int(*compar)(const void* ptr1, const void* ptr2));
//int removeFlightData_index(FlightDataNode* root_ptr, int index);
//int removeFlightData(FlightDataNode* root_ptr, FlightData* rm_data);

#endif
