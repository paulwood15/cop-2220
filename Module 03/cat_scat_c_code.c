#include <avr/sleep.h>

int buzzerOutputPin = 9 ;        // Connected to speaker
int lightSensorChannel = 0 ;     // input from LDR (CdS cell) - goes to A/D converter
int ledOutputPin = 12 ;          // Turns on the green LED
int solenoidOutputPin = 11 ;     // Fires the spray can
int irSensorPin = 10 ;           // input from PIR sensor - detects heat from cat
int enableSprayPin = 13 ;        // Only activate the spray if this is low.

unsigned int numSprays = 0 ;     // keep track of how many times sprayed

// Handle watchdog timer interrupt
// Need this because this is the interrupt
// that wakes us up from sleep mode.
ISR(SIG_WATCHDOG_TIMEOUT)
{
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  WDTCSR = 0;
}

// Based on http://wiki.engr.uiuc.edu/display/ae498mpa/Making+a+Police+Speed-Trap+Logger+Using+GPS
// For details, please see the Atmel ATMega168 data sheet, 
// http://www.atmel.com/dyn/resources/prod_documents/doc2545.pdf

void sleep_sec(uint8_t x)
{
  while (x--)
  {
     // set the WDT to wake us up!
    WDTCSR |= (1 << WDCE) | (1 << WDE); // enable watchdog & enable changing it
    
    // 1 second sleep
    WDTCSR = (1<< WDE) | (1 <<WDP2) | (1 << WDP1);
    
    // Sleep 8 seconds per.
    //WDTCSR = (1<< WDE) | (1 <<WDP3) | (1 << WDP0);
    WDTCSR |= (1<< WDIE);
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_enable();
    sleep_mode();
    sleep_disable();
  }
}

// Return 1 if the light level is below the darkValue,
// otherwise return 0.
// Average numSamples samples.  
int isDark ( int numSamples, int darkValue )
{
  int i ;
  long sampleRead = 0L ;
 
  for ( i = 0 ; i < numSamples ; i++ )
  {
    sampleRead += ( long ) analogRead ( lightSensorChannel ) ;
  }
  
  sampleRead = ( 10L * sampleRead ) / numSamples ;
 
  if ( sampleRead > ( darkValue * 10 ) )
  {
    return 1 ;
  }
  else
  {
    return 0 ;
  }
}

// Step through the states that constitue our
// functionality.  This compresses a lot of if/else's into
// a single function using switch.
//
// State 0 - initial state: sleep and wait for sensor to trigger
// State 1 - sensor was tripped, flash the LED and wait for a further trigger
// State 2 - kill time waiting for deadtime to expire, prevents triggering too frequently.
void stateMachine ( int sensorInput )
{
  static int state = 0 ;
  static int triggerStartTime ;
  static int deadStartTime ;
  static int warningTime = 7000 ;
  static int deadTime = 10000 ;
  int currentTime ;
  int dark ;
  
  // Leftover debugging code
  //Serial.print ( "State = " ) ;
  //Serial.print ( state ) ;
  //Serial.print ( ", sensor = " ) ;
  //Serial.println ( sensorInput ) ;
  
  dark = isDark ( 5, 950 ) ;
  switch ( state )
  {
    // Waiting for trigger
    case 0:
      // It's dark and there's a cat -- maybe.  Start blinks for pre-shot warning
      if ( sensorInput && dark )
      {
        state = 1 ;
        blinkLed ( 250, ledOutputPin ) ;
        triggerStartTime = millis ( ) ;
      }
      else
      {
        // Sleep 1 second to save battery power
        sleep_sec ( 1 ) ;
      }
    break ;
    
    // Keep flashing the warning light, start timing
    case 1:
      blinkLed ( 250, ledOutputPin ) ;
      currentTime = millis ( ) ;
      if ( ( currentTime - triggerStartTime ) > warningTime )
      {
        // Cat still there after warningTime seconds?  Still dark?
        if ( sensorInput && dark )
        {
          beep ( 3000 ) ;
          
          // Zap the cat with the spray one time in four (average)
          if ( sprayEnabled ( enableSprayPin ) )
          {
            long doSpray = random ( 10 ) ;
            if ( doSpray > 4L )
            {
              sprayCat ( 100, solenoidOutputPin ) ;
            }
          }
          numSprays ++ ;
          Serial.print ( "Sprays = " ) ;
          Serial.println ( numSprays ) ;
          state = 2 ;
          deadStartTime = millis ( ) ;
          digitalWrite ( ledOutputPin, HIGH ) ;
        }
        else
        {
          state = 0 ;
          digitalWrite ( ledOutputPin, LOW ) ;
        }  
      }
    break ;
    
    // Dead time - prevents triggering over and over and
    // using up all the gas on stubborn cats or false alarms
    case 2:
      currentTime = millis ( ) ;
      if ( ( currentTime - deadStartTime ) > deadTime )
      {
        digitalWrite ( ledOutputPin, LOW ) ;
        state = 0 ;
      }
    break ;
    
    default:
    break ;
  }
}

// See if the jumper is set to allow spraying --
// otherwise you just get flashing
int sprayEnabled ( int sprayEnablePin ) 
{
  int result = 0 ;
  if ( digitalRead ( sprayEnablePin ) == LOW )
  {
    result = 1 ;
  }
  return result ;
}

// Activate the sprayer for the specified number of milliseconds
// Originally the sprays were WAY too long (5 seconds) and 
// cat ran around the room in a disruptive manner trying to escape.
void sprayCat ( int sprayDuration, int sprayPin )
{
    digitalWrite ( sprayPin, HIGH ) ;
    delay ( sprayDuration ) ;
    digitalWrite ( solenoidOutputPin, LOW ) ;
    Serial.println ( "Psssst!" ) ;
}

// Blink the warning LED with the duty cylce of blinkDelay milliseconds
// Thank you, Pavlov.
void blinkLed ( int blinkDelay, int ledPin )
{
  static int blinkState = 0 ;
  static int blinkTime = 0 ;
  
  int currTime = millis ( ) ;
  if ( ( currTime - blinkTime ) > blinkDelay )
  {
    blinkTime = currTime ;
    if ( blinkState )
    {
      digitalWrite ( ledPin, HIGH ) ;
    }
    else
    {
      digitalWrite ( ledPin, LOW ) ;
    }
    blinkState = !blinkState ;
  }
}

// Generate approximately 1khz tone of duration ms.
// Making sound the hard way.  This is colloquially known
// as "bit-banging".
void beep ( int duration )
{
  int startTime = millis ( ) ;
  int currTime ;
 
  while ( ( currTime - startTime ) < duration )
  {
    digitalWrite ( buzzerOutputPin, HIGH ) ;
    delayMicroseconds ( 500 ) ;
    digitalWrite ( buzzerOutputPin, LOW ) ;
    delayMicroseconds ( 500 ) ;
    currTime = millis ( ) ;
  }
}

// One time initialization goes here
void setup ( )
{
  // Say which pins are outputs, and which are inputs
  pinMode ( ledOutputPin, OUTPUT ) ;
  pinMode ( solenoidOutputPin, OUTPUT ) ;
  pinMode ( buzzerOutputPin, OUTPUT ) ;
  pinMode ( irSensorPin, INPUT ) ;
  
  // enable internal pullup.  Jumper grounds
  // this pin to enable spraying
  pinMode ( enableSprayPin, INPUT ) ;
  digitalWrite ( enableSprayPin, HIGH ) ;
  
  // Start everything in a safe state -- don't spray
  // when reset, or when battery changed.
  digitalWrite ( solenoidOutputPin, LOW ) ;
  digitalWrite ( ledOutputPin, LOW ) ;
  digitalWrite ( buzzerOutputPin, LOW ) ;
  
  // Init watchdog timer
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  WDTCSR = 0;
  
  Serial.begin ( 9600 ) ;
  Serial.println ( "starting" ) ;
}

// Main program
void loop ( )
{
  stateMachine ( digitalRead ( irSensorPin ) ) ;
}


