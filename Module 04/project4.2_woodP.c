
/*
Paul Wood 
Due: 02/11/2018
Last Updated: 02/11/2018

Project 4.2

Comments:
*I thought I would add a little to this conditional project to add some randomness to it and to practice some other areas in C.
The program goes through a very, very simple football game simulation. It gives a team 4 "tries" to score - which is random
and the score amount is random as well. The specific conditional for this project is run in two different places:
in the "score()" (line 128) and "main()" (line 338). The conditonal function is on line 176.
*I wont use so many global variables next time...
*had to use secure functions because VS kept on yelling at me >:(
*/



#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#define STR_LENGTH 51			//max length for names of teams/stadium
#define CHANCE_DEFAULT 0.05
#define SEC_IN_QUARTER 900		//15 min per quarter -- 1 hour game

//Game states
enum BallPossession {POS_HOME, POS_AWAY};

enum Score {FieldGoal = 3, TD = 7};

//team names and scores
int awayScore;
char awayName[STR_LENGTH];

int homeScore;
char homeName[STR_LENGTH];

//where the game is held
char stadiumName[STR_LENGTH];

//info about the progression of the game
int timeSec = SEC_IN_QUARTER;		
bool gameIsWon = false;
int scoreChance = CHANCE_DEFAULT;

enum BallPossession ballPos;

struct GameEvent {
	enum BallPossession bp;
	int quarter;
	int timestamp;
	int scored;
};


struct GameEvent events[1000];
int nextEvent = 0;

void checkIfWin(int quarter);


//prints the stats of the game so far
void printStats(int qtr) {

	printf("\n=========================================================================\n");
	printf("%d sec left in quarter	quarter:%d\n", timeSec, qtr);
	printf("Home: %d    Away: %d\n", homeScore, awayScore);
	printf("=========================================================================\n\n");

}


//creates a new event in array
void newEvent(enum BallPossession _bp, int qtr,int sc) {
	events[nextEvent].bp = _bp;
	events[nextEvent].quarter = qtr;
	events[nextEvent].timestamp = timeSec;
	events[nextEvent].scored = sc;
	printf("new event: bp: %d, qtr: %d, ts: %d, sc: %d\n", _bp, qtr, timeSec, sc);
	nextEvent++;
}


//if the team scores, a random score type is given to the scoring team
void score(int qtr) {
	
	//touch down
	if (rand() % 100 < 50) {
		
		//home scores
		if (ballPos == POS_HOME) {
			homeScore += TD;
			printf("\n%s scored a touchdown!\n\n", homeName);
			newEvent(POS_HOME, qtr, TD);
		}

		//away scores
		else {
			awayScore += TD;
			printf("\n%s scored a touchdown!\n\n", awayName);
			newEvent(POS_AWAY, qtr, TD);
		}
	}
	//field goal
	else {
		
		//home scores
		if (ballPos == POS_HOME) {
			homeScore += FieldGoal;
			printf("\n%s scored a field goal!\n\n", homeName);
			newEvent(POS_HOME, qtr, FieldGoal);
		}

		//away scores
		else {
			awayScore += FieldGoal;
			printf("\n%s scored a field goal!\n\n", awayName);
			newEvent(POS_AWAY, qtr, FieldGoal);
		}
	}

	checkIfWin(qtr);
}


//decrements the game clock by a random amount
void decrementClock(int qtr) {
	int timeDecr;

	if ((timeSec < 60) && (qtr == 4)) {
		timeDecr = rand() % 10;
	}
	else {
		timeDecr = rand() % 100;
	}

	if (timeSec - timeDecr > 0) {
		timeSec -= timeDecr;
	}
	else {
		timeSec = 0;
	}
}


//sets up a new game with params
void newGame() {
	awayScore = 0;
	homeScore = 0;

	//Announcer starts off the game
	printf("Welcome to the %s stadium! \nAfter the coin flip, ", stadiumName);
	if ((rand() % 2) > 0) {
		ballPos = POS_HOME;
		printf("%s will be kicking the ball.\n\n", awayName);
	}
	else {
		ballPos = POS_AWAY;
		printf("%s will be kicking the ball.\n\n", homeName);
	}

}


//
//
//WINNING CONDITION
//
//
void checkIfWin(int quarter) {
	
	if ((quarter == 4) && (timeSec <= 20)) {
		
		if ((timeSec == 0) && (homeScore == awayScore)) {
			gameIsWon = true;
			printf("\n\n\n%s and %s tie!!\n\n\n", homeName, awayName);
			printStats(quarter);
		}
		//home up by 1 TD or more
		else if ((homeScore >= awayScore + 7) && (events[nextEvent - 1].timestamp < 21)) {
			//away team turned it over 
			if ((events[nextEvent - 1].scored == 0) && (events[nextEvent - 1].bp == 1)) {
				gameIsWon = true;
				printf("turn over - win default\n");
				printf("\n\n\n%s wins!!\n\n\n", homeName);
				printStats(quarter);
			}
			//home team scores a FG or TD again
			else if (((events[nextEvent - 1].scored == TD) || (events[nextEvent - 1].scored == FieldGoal)) && (events[nextEvent - 1].bp == 0)) {
				gameIsWon = true;
				printf("home scored again - win default\n");
				printf("\n\n\n%s wins!!\n\n\n", homeName);
				printStats(quarter);
			}
			//away team only scores a FG
			else if ((events[nextEvent - 1].scored == FieldGoal) && (events[nextEvent - 1].bp == 1)) {
				gameIsWon = true;
				printf("away only scored FG - win default\n");
				printf("\n\n\n%s wins!!\n\n\n", homeName);
				printStats(quarter);
			}

		}
		//away up 1 TD or more
		else if ((awayScore >= homeScore + 7) && (events[nextEvent - 1].timestamp < 21)) {
			//home team turned it over 
			if ((events[nextEvent - 1].scored == 0) && (events[nextEvent - 1].bp == 0)) {
				gameIsWon = true;
				printf("turn over - win default\n");
				printf("\n\n\n%s wins!!\n\n\n", awayName);
				printStats(quarter);
			}
			//home team scores a FG or TD again
			else if (((events[nextEvent - 1].scored == TD) || (events[nextEvent - 1].scored == FieldGoal)) && (events[nextEvent - 1].bp == 1)) {
				gameIsWon = true;
				printf("away scored again - win default\n");
				printf("\n\n\n%s wins!!\n\n\n", awayName);
				printStats(quarter);
			}
			//home team only scores a FG
			else if ((events[nextEvent - 1].scored == FieldGoal) && (events[nextEvent - 1].bp == 0)) {
				gameIsWon = true;
				printf("away only scored FG - win default\n");
				printf("\n\n\n%s wins!!\n\n\n", awayName);
				printStats(quarter);
			}

		}

		//if it's a close game - home
		else if  ((timeSec == 0) && (homeScore > awayScore)) {
			gameIsWon = true;
			printf("to the last second");
			printf("\n\n\n%s wins!!\n\n\n", homeName);
			printStats(quarter);
		}
		//if it's a close game - away
		else if ((timeSec == 0) && (homeScore < awayScore)) {
			gameIsWon = true;
			printf("to the last second");
			printf("\n\n\n%s wins!!\n\n\n", awayName);
			printStats(quarter);
		}
		
		//if it's within the last 20s and the above wasnt met - the away team won
		/*
		else {
		gameIsWon = true;
		printf("\n\n\n%s wins!!\n\n\n", awayName);

		printStats(quarter);
		}
		*/
	}


	//END CONDITION
}

int main(void) {
	srand((int)time(0));

	printf("Football - names must be a single word\n");
	printf("Home team: ");
	scanf_s("%s", &homeName, STR_LENGTH);

	printf("Away team: ");
	scanf_s("%s", &awayName, STR_LENGTH);

	printf("Stadium: ");
	scanf_s("%s", &stadiumName, STR_LENGTH);

	
	newGame("jags", "steelers", "bleh");
	int randVal = 0;

	//4 quarters
	for (int quarter = 1; quarter <= 4; quarter++) {
		
		printf("\n\nQuarter: %d\n\n\n", quarter);

		while ( ((timeSec > 0 && quarter < 4) || (timeSec >= 0 && quarter == 4)) && !(gameIsWon) ) {

			//generates a score chance per 4 downs 
			scoreChance = rand() % 50;

			for (int down = 1; down <= 4; down++) {
			
				//check if game is won or ended
				if (gameIsWon || timeSec == 0) {
					break;
				}

				//announcer
				printf("%s now have the ball.\n", (ballPos == 0) ? homeName : awayName);
				

				//if the randVal is below the score chance, the team scores
				randVal = rand() % 100;
				if ( (timeSec > 0) && (randVal < scoreChance) ) {
					score(quarter);
					break;
				}
				else {
					printf("%s try but the %s stop them!\tdown:%d\n", 
										(ballPos == 0) ? homeName : awayName,
										(ballPos == 1) ? homeName : awayName,
										 down);

				}

				if (down == 4) {
					printStats(quarter);
				}
				
				//printf("\ndecre\n");

				//decrements clock to simulate a play
				decrementClock(quarter);

				
			}	

			//checked again to avoid getting turn over edge case every time on top of another case
			if (gameIsWon) {
				break;
			}

			//switch ball possession 
			ballPos = (ballPos == POS_HOME) ? POS_AWAY : POS_HOME;
			newEvent(ballPos, quarter, 0);
			checkIfWin(quarter);
		}

		timeSec = SEC_IN_QUARTER;
	}

	system("pause");
}
