#include <stdio.h>
#include <stdio.h>
#include <ctype.h>
#include <stdbool.h>
#include "dirent.h"


#define MAX_FILES 800
#define DIR_SIZE 261
#define MAX_CHARS 100
#define FILE_LEN 50


typedef struct IntNode IntNode;
typedef struct Node Node;
typedef struct File_Statistics ProgramStatistics;

//function prototypes
int isDirectory(const char *path);
int traverseDIRFor(char rootDirName[], FILE* file, const char fileExt[], int *numMatchedFiles);
bool isKeyword(ProgramStatistics* stat, char s[]);



struct File_Statistics{
	char file_name[FILE_LEN];
	int total_elements;
	
	//non-compileed 
	int num_lines;
	int num_blank_lns;
	int num_comments;

	//data types	
	int num_ints;
	int num_longs;
	int num_floats;
	int num_doubles;
	int num_chars;

	//keywords
	int num_ifs;
	int num_elses;
	int num_fors;
	int num_whiles;
	int num_switches;
	int num_structs;

	//program structure
	int num_arrays;
	int num_blocks;
};



//TODO: better and more open ended file extension searcher

/*
Checks to see if the file path is a directory
Params
	path: the object you would like to check
Returns
	0: if it is not
	1: if it is 
*/
int isDirectory(const char *path) {
	struct stat statbuf;
	if (stat(path, &statbuf) != 0)
		return 0;
	return S_ISDIR(statbuf.st_mode);
}

/*
Traverses a directory and subdirectories returning all files with a certain file extension.
Params:
	rootDirName: name of the directory the traversal is rooting from
	fp: array of file paths
	fileExt: a string of the file extension to be searched (ex. (.c)) MUST include '.'
	numMatchedFiles: the number of matched files so far
Returns
	-1: cannot open root DIR
	 0: successful
*/
int traverseDIRFor(char rootDirName[], char **fp, const char fileExt[], int *numMatchedFiles) {
	//pointer with current directory and pointers to a struct with details on the directory
	DIR *rootDir = opendir(rootDirName);
	

	//printf("looking for %s files in %s\n\n\n", fileExt, rootDirName);
	//system("pause");

	struct dirent *de;  // Pointer for directory entry
	
	//test if the starting DIR is NULL
	if (rootDir == NULL)
	{
		//printf("Could not open starting directory");
		return -1;
	}

	while ((de = readdir(rootDir)) != NULL) {

		//printf("%s\n", de->d_name);
		char new_path[DIR_SIZE];
		strcpy_s(new_path, DIR_SIZE, rootDirName);
		strcat_s(new_path, DIR_SIZE, "/");
		strcat_s(new_path, DIR_SIZE, de->d_name);
		//printf_s("dir set: %s\n", new_path);


		//check for unwanted folders "." and ".." .....				o.o no idea what their purpose is
		if ( (strcmp(de->d_name, ".") == 0) || (strcmp(de->d_name, "..") == 0) ) {
			//printf("unwanted\n");
			continue;
		}

		//file extension matches COMPLETELY
		if ((strstr(de->d_name, fileExt) != NULL) && (de->d_name[de->d_namlen - strlen(fileExt)] == fileExt[0])) {
			//printf("Found %s file!\n", fileExt);

			//printf("%d\n", *numMatchedFiles);
			strcpy_s(fp[*numMatchedFiles], DIR_SIZE, new_path);
			(*numMatchedFiles)++;
			//printf("\n\nnum files found: %d\n\n", *numMatchedFiles);
		}
		else if (isDirectory(new_path) > 0) {
			//printf("Found sub-directory: %s\n", new_path);
			traverseDIRFor(new_path, fp, fileExt, numMatchedFiles);
		}
		else {
			//not directory &&|| file not being searched for
		}
		
	}

	closedir(rootDir);

	return 0;
}


bool searchExact(char str[], char test[]) {
	bool is_exact = false;
	char* ptr = strstr(str, test);
	if (ptr != NULL) {
		if (*ptr == str[0]) {
			if (!(isalnum(*(ptr + strlen(test))))) {
				is_exact = true;
			}
		}
		else {
			if (!(isalnum(*(ptr - 1))) && !(isalnum(*(ptr + strlen(test))))) {
				is_exact = true;
			}
		}
	}

	return is_exact;
}
	

void getStatistics(ProgramStatistics* stat, const char file_path[]) {
	FILE* file_read;
	fopen_s(&file_read, file_path, "r");
	if (!file_read)
	{
		printf("unable to open file: %s\n", file_path);
		return;
	}
	bool inComment = false;
	char next_line[MAX_CHARS] = { NULL };
	while (fgets(next_line, MAX_CHARS, file_read) != NULL) {
		//if currently in multi-line comment - ignore
		if (inComment) {
			if (strstr(next_line, "*/") != NULL) {
				inComment = false;
			}
			else {
				stat->num_lines++;
				continue;
			}
		}
		//look for blank line
		if (next_line[0] == '\n') {
			stat->num_blank_lns++;
			stat->num_lines++;
			continue;
		}
		//look for single line comment 
		if (strstr(next_line, "//") != NULL) {
			stat->num_comments++;
			stat->num_lines++;
			continue;
		}
		//look for multi-line comment
		if (strstr(next_line, "/*") != NULL) {
			stat->num_comments++;
			stat->num_lines++;\
			inComment = true;
			continue;
		}

		//look for array
		if (strchr(next_line, '[') != NULL) {
			stat->num_arrays++;
			stat->total_elements++;
		}
		//look for block
		if (strchr(next_line, '{') != NULL) {
			stat->num_blocks++;
		 }
		//look for char var type
		if (searchExact(next_line, "char")) {
			stat->num_chars++;
			stat->total_elements++;
		}
		//look for double var type
		if (searchExact(next_line, "double")) {
			stat->num_doubles++;
			stat->total_elements++;
		}
		//look for float var type
		if (searchExact(next_line, "float")) {
			stat->num_floats++;
			stat->total_elements++;
		}
		//look for int var type
		if (searchExact(next_line, "int")) {
			stat->num_ints++;
			stat->total_elements++;
		}
		//look for long var type
		if (searchExact(next_line, "long")) {
			stat->num_longs++;
			stat->total_elements++;
		}
		//look for for loop
		if (searchExact(next_line, "for")) {
			stat->num_fors++;
			stat->total_elements++;
		}
		//look for while loop
		if (searchExact(next_line, "while")) {
			stat->num_whiles++;
			stat->total_elements++;
		}
		//look for if statement
		if (searchExact(next_line, "if")) {
			stat->num_ifs++;
			stat->total_elements++;
		}
		//look for else statement
		if (searchExact(next_line, "else")) {
			stat->num_elses++;
			stat->total_elements++;
		}
		//look for struct
		if (searchExact(next_line, "struct")) {
			stat->num_structs++;
			stat->total_elements++;
		}
		//look for switch
		if (searchExact(next_line, "switch")) {
			stat->num_switches++;
			stat->total_elements++;
		}

		stat->num_lines++;
	}

	fclose(file_read);
}

void initStat(ProgramStatistics* stat, char program_name[]) {
	strcpy_s(stat->file_name, FILE_LEN, program_name);
	stat->total_elements = 0;
	stat->num_arrays = 0;
	stat->num_blank_lns = 0;
	stat->num_blocks = 0;
	stat->num_chars = 0;
	stat->num_comments = 0;
	stat->num_doubles = 0;
	stat->num_elses = 0;
	stat->num_floats = 0;
	stat->num_fors = 0;
	stat->num_whiles = 0;
	stat->num_ifs = 0;
	stat->num_ints = 0;
	stat->num_lines = 0;
	stat->num_longs = 0;
	stat->num_structs = 0;
	stat->num_switches = 0;
}

void printStats(ProgramStatistics* fs) {
	printf("file statistics: %s\n", fs->file_name);
	printf("-------------------------------\n");
	printf("num lines:\t %d\n", fs->num_lines);
	printf("num bnk lines:\t %d\n", fs->num_blank_lns);
	printf("-------------------------------\n");
	printf("num blocks:\t %d\n", fs->num_blocks);
	printf("num arrays:\t %d\t(%.2f%%)\n", fs->num_arrays, ((float)fs->num_arrays * 100) / fs->total_elements);
	printf("num comments:\t %d\t(%.2f%%)\n", fs->num_comments, ((float)fs->num_comments * 100) / fs->total_elements);
	printf("-------------------------------\n");
	printf("num fors:\t %d\t(%.2f%%)\n", fs->num_fors, ((float)fs->num_fors * 100) / fs->total_elements);
	printf("num whiles:\t %d\t(%.2f%%)\n", fs->num_whiles, ((float)fs->num_whiles * 100) / fs->total_elements);
	printf("num if:\t\t %d\t(%.2f%%)\n", fs->num_ifs, ((float)fs->num_ifs * 100) / fs->total_elements);
	printf("num else:\t %d\t(%.2f%%)\n", fs->num_elses, ((float)fs->num_elses * 100) / fs->total_elements);
	printf("num structs:\t %d\t(%.2f%%)\n", fs->num_structs, ((float)fs->num_structs * 100) / fs->total_elements);
	printf("num switches:\t %d\t(%.2f%%)\n", fs->num_switches, ((float)fs->num_switches * 100) / fs->total_elements);
	printf("-------------------------------\n");
	printf("num int:\t %d\t(%.2f%%)\n", fs->num_ints, ((float)fs->num_ints * 100) / fs->total_elements);
	printf("num doubles:\t %d\t(%.2f%%)\n", fs->num_doubles, ((float)fs->num_doubles * 100) / fs->total_elements);
	printf("num floats:\t %d\t(%.2f%%)\n", fs->num_floats, ((float)fs->num_floats * 100) / fs->total_elements);
	printf("num longs:\t %d\t(%.2f%%)\n", fs->num_longs, ((float)fs->num_longs * 100) / fs->total_elements);
	printf("num char:\t %d\t(%.2f%%)\n", fs->num_chars, ((float)fs->num_chars * 100) / fs->total_elements);
}

//TODO: traverse with custom search using pointing function
int main(void)
{
	
	//init
	int p1_r = 280;
	char **file_paths_quake = malloc(p1_r * sizeof(char*));
	for (int r = 0; r < p1_r; r++) {
		file_paths_quake[r] = malloc(DIR_SIZE * sizeof(char));
	}

	//Program 1
	char program1[] = "./Programs/Quake-2-master";
	ProgramStatistics quake_statistics;
	int p1_num_c_files = 0;
	int p1_num_h_files = 0;
	printf("loading %s...\n", program1);
	printf("locating all program files...\n\n");

	traverseDIRFor(program1, file_paths_quake, ".c", &p1_num_c_files);
	p1_num_h_files = p1_num_c_files;
	traverseDIRFor(program1, file_paths_quake, ".h", &p1_num_h_files);
	p1_num_h_files -= p1_num_c_files;

	
	printf("traversing completed.\n");
	printf("getting statistics...\n\n\n\n");

	initStat(&quake_statistics, "Quake 2 (.c and .h)");
	for (int i = 0; i < (p1_num_c_files + p1_num_h_files); i++) {
		getStatistics(&quake_statistics, file_paths_quake[i]);
	}
	
	printStats(&quake_statistics);

	//cleanup
	for (int i = 0; i < p1_r; i++) {
		free(file_paths_quake[i]);
	}
	free(file_paths_quake);

	printf("\n");

	/*ProgramStatistics stat_test;
	initStat(&stat_test, "test");
	getStatistics(&stat_test, "C:\\Users\\Paul Wood\\Google Drive\\School\\2018 Spring\\C Programming\\Module 06\\ProgramStats\\ProgramStats\\Programs\\tensorflow-master\\tensorflow\\examples\\learn\\multiple_gpu.py");
	printStats(&stat_test);*/

	//Program 2
	int p2_r = 10000;
	char **file_paths_tf = malloc(p2_r * sizeof(char*));
	for (int r = 0; r < p2_r; r++) {
		file_paths_tf[r] = malloc(DIR_SIZE * sizeof(char));
	}
	
	ProgramStatistics tensorflow_statistics;
	char program2[] = "./Programs/tensorflow-master/tensorflow";
	int p2_num_py_files = 0;
	int p2_num_cc_files = 0;

	printf("loading %s...\n", program2);
	printf("locating all program files...\n\n");

	traverseDIRFor(program2, file_paths_tf, ".py", &p2_num_py_files);
	p2_num_cc_files = p2_num_py_files;
	traverseDIRFor(program2, file_paths_tf, ".cc", &p2_num_py_files);
	p2_num_cc_files -= p2_num_py_files;

	printf("traversing completed.\n");
	printf("getting statistics...\n\n\n\n");

	initStat(&tensorflow_statistics, "Tensorflow (.py and .cc)");
	for (int i = 0; i < (p2_num_py_files + p2_num_cc_files); i++) {
		getStatistics(&tensorflow_statistics, file_paths_tf[i]);
	}

	printStats(&tensorflow_statistics);

	//cleanup
	for (int i = 0; i < p2_r; i++) {
		free(file_paths_tf[i]);
	}
	free(file_paths_tf);

	system("pause");
	return 0;
}

