/* 
Paul Wood 
02/03/2018
*/

// Test C wrapper for simple svg classes

// Thjs example works, but need to check how the screen coordinates
// are figured....

// For a refernece on SVG capabilities, see: http://www.svgbasics.com/using_fonts.html

// Include prototypes for our functions.
// The functions in turn call on the methods of classes and
// objects written in C++.

#include "svgFunctions.h"
#include <stdbool.h>
#include <time.h>
#include <math.h>


#define PI 3.1415926535

//works best with 3/4 ratios
#define DOC_Y 1200
#define DOC_X 1600
#define CUT_OFF 5 


//x=0; y=docY are is the top most coord of the page 
int main(int argc, char* argv[])
{
	srand((int)time(0));
	void* docPtr = NULL;

	int eye1X = DOC_X / 4;
	int eye2X = DOC_X * 3 / 4;
	int eye12Y = DOC_Y * 2 / 3;


	//background vars
	int subDivX = 1;//rand() % 10 + 1;
	int subDivY = rand() % 30 + 15;
	int numSubDivsX = DOC_X / subDivX;
	int numSubDivsY = DOC_Y / subDivY;

	//eye vars 
	int startX;
	int startY;
	int endX;
	int endY;
	int numRotations = 13;
	int rotInDeg = numRotations * 360;
	int deltaTheta = 20;
	int radius = 2;
	int thickness = 5;
	double multiplier = 0.5;
	colorType eyeColor = COLOR_WHITE;



	printf("Generating...");
	docPtr = initialize((char*)"test_svg.svg", DOC_X, DOC_Y, (char*)"white");

	//background
	colorType rectColor;
	for (int i = 0; i < numSubDivsX; i++) {
		for (int j = 0; j < numSubDivsY; j++) {
			rectColor = static_cast<colorType>(rand() % 4);
			drawRectangle(docPtr, i * subDivX, subDivY * (j + 1), subDivY, subDivX, rectColor, rectColor);
		}
	}

	//eye 1
	startX = eye1X;
	startY = eye12Y;
	for (int i = 0; i < rotInDeg; i += deltaTheta) {
		endX = eye1X + (int)((radius * i / deltaTheta) * multiplier * cos((i + deltaTheta) * PI / 180));
		endY = eye12Y + (int)((radius * i / deltaTheta) * multiplier * sin((i + deltaTheta) * PI / 180));

		drawLine(docPtr, thickness, startX, startY, endX, endY, eyeColor);

		startX = endX;
		startY = endY;
	}

	//eye 2
	startX = eye2X;
	startY = eye12Y;
	for (int i = 0; i < rotInDeg; i += deltaTheta) {
		endX = eye2X + (int)((radius * i / deltaTheta) * multiplier * cos((i + deltaTheta) * PI / 180));
		endY = eye12Y + (int)((radius * i / deltaTheta) * multiplier * sin((i + deltaTheta) * PI / 180));

		drawLine(docPtr, thickness, startX, startY, endX, endY, eyeColor);

		startX = endX;
		startY = endY;
	}


	//mouth
	drawText(docPtr, DOC_X * 0.28, DOC_Y * 0.3, 200, COLOR_WHITE, (char*)"Terminal Regular", (char*)"01001000");
	drawText(docPtr, DOC_X * 0.28, DOC_Y * 0.19, 200, COLOR_WHITE, (char*)"Terminal Regular", (char*)"01101001");

	// Close and flush the XML file with the SVG commands
	saveDocument(docPtr);
	printf("Done\n");

	system("pause");
	return 0;
}

