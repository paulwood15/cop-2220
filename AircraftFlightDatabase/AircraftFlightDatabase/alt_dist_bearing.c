/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: jerryreed
 *
 * Created on March 21, 2018, 4:19 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Point on globe, like Orlando
typedef struct 
    {
        double lat ;
        double lon ;
        double elv ;
    } Location ;
    
    // Used internally in calculateBearing()
    typedef struct 
    {
        double x ;
        double y ;
        double z ;
        double radius ;
    } Coords ;
    
    // Bearing to a point from a point, like home to plane
    typedef struct
    {
        double elevation ;
        double distance ;
        double azimuth ;
    } Fix ;
    
double feetToMeters ( double feet )
{
	return feet / 3.2808 ;
}

double kmToMiles ( double km ) 
{
	return km * 0.62137 ;
}

double EarthRadiusInMeters ( double latitudeRadians)
{
    // http://en.wikipedia.org/wiki/Earth_radius
    double a = 6378137.0;  // equatorial radius in meters
    double b = 6356752.3;  // polar radius in meters
    double cosx = cos (latitudeRadians);
    double sinx = sin (latitudeRadians);
    double t1 = a * a * cosx;
    double t2 = b * b * sinx;
    double t3 = a * cosx;
    double t4 = b * sinx;
    return sqrt ((t1*t1 + t2*t2) / (t3*t3 + t4*t4));
}

Coords LocationToPoint (Location c )
{
    // Convert (lat, lon, elv) to (x, y, z).
    Coords retCoord ;

    double lat = c.lat * M_PI / 180.0;
    double lon = c.lon * M_PI / 180.0;
    retCoord.radius = c.elv + EarthRadiusInMeters (lat);
    double cosLon = cos (lon);
    double sinLon = sin (lon);
    double cosLat = cos (lat);
    double sinLat = sin (lat);
    retCoord.x = cosLon * cosLat * retCoord.radius;
    retCoord.y = sinLon * cosLat * retCoord.radius;
    retCoord.z = sinLat * retCoord.radius;
    return retCoord ;
}

double Distance ( Coords ap, Coords bp )
{
    double dx = ap.x - bp.x;
    double dy = ap.y - bp.y;
    double dz = ap.z - bp.z;
    return sqrt (dx*dx + dy*dy + dz*dz);
}

Coords RotateGlobe (Location b, Location a, double bradius, double aradius)
{
    // Get modified coordinates of 'b' by rotating the globe so that 'a' is at lat=0, lon=0.
    Location br ;
    Coords retCoord ;

    br.lat = b.lat ;
    br.lon = b.lon - a.lon ;
    br.elv = b.elv ;

    Coords brp = LocationToPoint ( br );

    // scale all the coordinates based on the original, correct geoid radius...
    brp.x *= (bradius / brp.radius);
    brp.y *= (bradius / brp.radius);
    brp.z *= (bradius / brp.radius);
    brp.radius = bradius;   // restore actual geoid-based radius calculation

    // Rotate brp cartesian coordinates around the z-axis by a.lon degrees,
    // then around the y-axis by a.lat degrees.
    // Though we are decreasing by a.lat degrees, as seen above the y-axis,
    // this is a positive (counterclockwise) rotation (if B's longitude is east of A's).
    // However, from this point of view the x-axis is pointing left.
    // So we will look the other way making the x-axis pointing right, the z-axis
    // pointing up, and the rotation treated as negative.

    double alat = -a.lat * M_PI / 180.0;
    double acos = cos (alat);
    double asin = sin (alat);

    retCoord.x = (brp.x * acos) - (brp.z * asin);
    retCoord.y = brp.y;
    retCoord.z = (brp.x * asin) + (brp.z * acos);

    return retCoord ;
}

double ParseAngle ( double angle, double limit )
{
    if (angle == 0.0 || (angle < -limit) || (angle > limit)) {
        return 0.0;
    } else {
        return angle;
    }
}

// Do we even need this function anymore?
double ParseElevation ( double angle )
{
    if ( angle == 0.0 ) {
        return 0.0 ;
    } else {
        return angle;
    }
}

Location ParseLocation ( double latStr, double lonStr, double elvVal )
{
    double lat = ParseAngle ( latStr, 90.0);
    Location location ;
    if ( lat != 0.0 ) {
        double lon = ParseAngle ( lonStr, 180.0);
        if (lon != 0.0 ) {
            double elv = elvVal ;
            if (elv != 0.0 ) {
                location.lat = lat ;
                location.lon = lon ;
                location.elv = elv ;
            }
        }
    }
    return location;
}

//
// Translated from JavaScript by Don Cross at
// http://cosinekitty.com/compass.html
//
Fix calculateBearing( Location observer, Location plane)
{
    Fix fix ;

    Location a = ParseLocation ( observer.lat, observer.lon, 
                feetToMeters (observer.elv ) ) ;
    
    Location b = ParseLocation ( plane.lat, plane.lon, 
                feetToMeters ( plane.elv ) ) ;

    Coords ap = LocationToPoint (a);
    Coords bp = LocationToPoint (b);
    double distKm = 0.001 * round( Distance (ap, bp) );
    fix.distance = kmToMiles ( distKm ) ;

    // Let's use a trick to calculate azimuth:
    // Rotate the globe so that point A looks like latitude 0, longitude 0.
    // We keep the actual radii calculated based on the oblate geoid,
    // but use angles based on subtraction.
    // Point A will be at x=radius, y=0, z=0.
    // Vector difference B-A will have dz = N/S component, dy = E/W component.

    Coords br = RotateGlobe (b, a, bp.radius, ap.radius);
    double theta = atan2 (br.z, br.y) * 180.0 / M_PI;
    double azimuth = 90.0 - theta;
    if (azimuth < 0.0) {
        azimuth += 360.0;
    }
    if (azimuth > 360.0) {
        azimuth -= 360.0;
    }
    fix.azimuth = round(azimuth*10)/10 ;
    //printf ( "azimuth=%lf,", (round(azimuth*10)/10) ) ;

   // $('div_Azimuth').innerHTML = (Math.round(azimuth*10)/10) + '&deg;';

    // Calculate altitude, which is the angle above the horizon of B as seen from A.
    // Almost always, B will actually be below the horizon, so the altitude will be negative.
    double shadow = sqrt ((br.y * br.y) + (br.z * br.z));
    double altitude = atan2 (br.x - ap.radius, shadow) * 180.0 / M_PI;
    fix.elevation = (round(altitude*100)/100) ;

    //printf ( "altitude=%lf\n", (round(altitude*100)/100) ) ;
    //$('div_Altitude').innerHTML = (Math.round(altitude*100)/100).toString().replace(/-/g,'&minus;') + '&deg;';

    return fix ;
}

/*
 * Test calcBearing( )
 */
int main(int argc, char** argv) {

    //                lat,     long,     elevation(feet)
    Location home = { 28.6934, -81.5322, 100 } ;
    Location plane = { 28.03134, -81.52373, 4358.64 } ;
    Fix theFix = calculateBearing( home, plane ) ;
    
    printf ( "azimuth=%lf, elevation=%lf, distance=%lf\n",
            theFix.azimuth, theFix.elevation, theFix.distance) ;
 
    return (EXIT_SUCCESS);
}

