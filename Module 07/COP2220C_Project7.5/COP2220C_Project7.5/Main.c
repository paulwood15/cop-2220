/*
Paul Wood
03/23/2017
COP2220C

Project 7.5

The purpose of this assignment is to learn how to read binary data into a structure and print it back. 
*/

#include <stdio.h>
#include <stdlib.h>

#define FLIGHT_NUM 7
#define AIRPORT_CODE 5
#define MAX

typedef struct FlightData_struct {
	char flight_num[FLIGHT_NUM];
	char origin_code[AIRPORT_CODE];
	char dest_code[AIRPORT_CODE];
	int timestamp;
}FlightData;

void printFlightData(FlightData* fd) {
	printf("%s\t%s\t%s\t%d\n", fd->flight_num, fd->origin_code, fd->dest_code, fd->timestamp);
}

int main(void) {

	FILE* airport_data;
	if (fopen_s(&airport_data, "./acars.bin", "rb") == 0) {
		//File opened successfully
	}
	else {
		printf("Error reading file!\n");
		system("pause");
		return -1;
	}

	printf("\t  FLIGHT DATA\n\n");
	printf("flight\torigin\tdest\ttimestamp\n");
	printf("---------------------------------\n");
	while (!feof(airport_data)) {
		FlightData data_read;
		int num_read = fread_s(&data_read, sizeof (data_read), sizeof(data_read), 1, airport_data);
		//printf("num items read: %d\n", num_read);
		printFlightData(&data_read);
	}
	system("pause");
}
