//Paul Wood
//04/08/2018
//Project 8.6 - creating and sorting arrays of structs



#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "FlightData.h"
#include "LinkedList.h"


int main(void) {

	/*LList* list = createList(15);
	int d1 = 69;
	int d2 = 3 * (int)'k';
	LL_insertAt(list, d1, 10);
	LL_printListContents(list);

	freeList(list);*/
	/*char test1[] = "KSRQ";
	char test2[] = "MMMX";
	char test3[] = "MMMX";
*/


	FILE *flight_data;
	LList* flight_records = createList(0);
	FlightData* data_read = (FlightData*)malloc(sizeof(FlightData));
	time_t posix;


	if (fopen_s(&flight_data, "./acars.bin", "rb") == 0) {
		//File opened successfully
	}
	else {
		printf("Error reading file!\n");
		system("pause");
		return -1;
	}

	int i = 0;
	while (fread(data_read, sizeof(FlightData), 1, flight_data) > 0) {
		LL_sortedInsert(flight_records, data_read, compare_DESTCODE);

		data_read = (FlightData*)malloc(sizeof(FlightData));
		i++;
	}


	//print everything in list
	Node* cursor = flight_records->head;
	while (cursor->next_ptr != NULL) {
		printFlightData(cursor->data);
		cursor = cursor->next_ptr;
	}

	printf("total files read: %d\n", i);
	printf("size of list: %d\n", flight_records->size);


	fclose(flight_data);
	system("pause");

	return 0;
}
