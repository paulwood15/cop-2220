/*
Paul Wood

*/


#pragma once
#include <stdbool.h>

#ifndef LINKEDLIST_H
#define LINKEDLIST_H

typedef struct Node_struct {
	void* data;
	struct Node_struct* next_ptr;
	struct Node_struct* prev_ptr;
}Node;

typedef struct LList_struct {
	Node* head;
	Node* tail;
	int size;

	//size of datatype being used in bytes
	int datatype_size;

	//helper variables for when indexing to avoid unnecessary searching
	Node* cursor;
	int cursor_index;
}LList;

//list and node creation
LList* createList(int pre_alloc_size);
Node* LL_createNode(void* data);
Node* blankNode(Node* node);

//list and node deletion
void freeList(LList* list);

//data inserters
void LL_pushBack(LList* list, void* data);
Node* LL_insertAfter(LList* list, void* data, Node* interest_node);
Node* LL_insertAt(LList* list, void* data, int index);
void LL_sortedInsert(LList* list, void* data, int(*compar)(const void* cmp1, const void* cmp2));
void LL_removeByLookup(LList* list, void* interest_data);
void LL_removeByPrevious(LList* list, void* interest_data);
void LL_removeByIndex(LList* list, void* interest_data);
void LL_printListPointers(LList* list);
void LL_printListContents(LList* list, const char* (*toString)(const void* data));

//data getters and settters
void* LL_getData(LList* list,int index);
Node* LL_getNode(LList* list, int index);
void LL_setData(LList* list, void* data, int index);



#endif