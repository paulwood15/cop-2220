/*
Paul Wood

*/

#include "LinkedList.h"
#include "FlightData.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>


/*
Creates a list with n number of empty elements.
Params:
	int, pre_alloc_size: number of empty elements to create
Returns:
	LList pointer: newly created list
*/
LList* createList(int pre_alloc_size)
{
	LList* list = (LList*)malloc(sizeof(LList));
	list->head = NULL;
	list->tail = NULL;
	list->cursor = list->head;
	list->cursor_index = 0;
	list->size = 0;

	for (int i = 0; i < pre_alloc_size; i++) {
		LL_pushBack(list, NULL);
	}

	printf("new list created @%p, size = %d\n", list, list->size);
	LL_printListPointers(list);

	return list;
}

/*
Creates a node with inputted data.
Params:
	void pointer, data: any data type casted to a void pointer
Returns:
	Node pointer: newly created node
*/
Node* LL_createNode(void* data)
{
	Node* node = (Node*)malloc(sizeof(Node));
	node->data = data;
	node->next_ptr = NULL;
	node->prev_ptr = NULL;

	return node;
}

/*
Empties node
Params:
	Node pointer, node: node to be cleared
Returns:
	Node pointer: cleared node
*/
Node* blankNode(Node* node)
{
	node->data = NULL;
	node->next_ptr = NULL;
	node->prev_ptr = NULL;
	
	return node;
}

void freeList(LList* list)
{
	Node* temp;

	while (list->head != NULL) {
		temp = list->head;
		list->head = (list->head)->next_ptr;
		free(temp);
	}

	free(list);
}


/*
Creates a node from inputted data and insert it at the end of the list. Will automatically set list 
head and tail, as well as size.
Params:
	LList pointer, list: list to insert data
	void pointer, data: data wished to be inserted into list. Must be casted to void pointer.
Returns:
	None
*/
void LL_pushBack(LList* list, void* data)
{
	Node* new_node = LL_createNode(data);

	//printf("before push: ");
	//LL_printListContents(list);

	if (list->size == 0) {
		list->head = new_node;
		list->tail = new_node;
		list->cursor = new_node;
	}
	else {
		(list->tail)->next_ptr = new_node;
		new_node->prev_ptr = list->tail;
		list->tail = new_node;
	}
	(list->size)++;

	//printf("after push: ");
	//LL_printListContents(list);
}


/*
Creates and inserts node using data after node inputted, and readjusts nodes.
Breaks current cursor location
Params:
	LList pointer, list: list for data to be inserted
	Node pointer, interest_node: node for data to be inserted after.
	void pointer, data: data to be inserted casted to a void pointer.
Returns:
	Node pointer: pointer to newly inserted node/data
*/
Node* LL_insertAfter(LList* list, void* data, Node* interest_node)
{
	//reset cursor
	list->cursor = list->head;
	list->cursor_index = 0;

	Node* new_node = LL_createNode(data);
	new_node->next_ptr = interest_node->next_ptr;
	new_node->prev_ptr = interest_node;
	interest_node->next_ptr = new_node;

	return new_node;
}


/*
Inserts data (creates new node) at specified index inside list. Will push node at current index to
index+1. This is why checking and readjusting list head is important. Insert at index==0 will cause original
list head to shift to a new index of 1.
Params:
	LList pointer, list: list to insert data
	void pointer, data: data to be inserted
	int, index: index for data to be inserted
Returns
	NULL: if index is out of bounds
	Node pointer: newly inserted node (with data) in list
*/
Node* LL_insertAt(LList* list, void* data, int index)
{
	Node* new_node = LL_createNode(data);			//new node for data


	if ((index >= list->size) || (index < 0)) {		//check for bound error
		printf("INDEX OUT OF BOUNDS\n");
		return NULL;
	}
	if (index == 0){								//check for if index==0 to avoid search
		new_node->next_ptr = list->head;			//tail check doesn't matter
		(list->head)->prev_ptr = new_node;
		list->head = new_node;
		list->cursor = list->head;
		list->cursor_index = 0;
	}
	else {
		Node* node_at = LL_getNode(list, index);
		new_node->next_ptr = node_at;
		new_node->prev_ptr = node_at->prev_ptr;
		(node_at->prev_ptr)->next_ptr = new_node;
		node_at->prev_ptr = new_node;
		list->cursor = new_node;
		list->cursor_index = index;
	}
	(list->size)++;

	printf("inserted data: p=%p\tindex=%d\n", new_node, index);
	return new_node;
}



/*
Used to sort as data is inserted into the list. Exsisting list must be sorted before using this function.
Params
	LList pointer, list: List for data to be inserted into
	void pointer, data: data to be inserted
	function pointer, compar: function to compare two pieces of data.
		REQUIREMENTS: --returns an int: negative for less-than, zero for equals, positive for greater-than
					  --return 0 for NULL data being passed through (only really for the beginning or end of list)
*/
void LL_sortedInsert(LList* list, void* data, int(*compar)(const void *cmp1, const void *cmp2))
{
	bool isInserted = false;
	list->cursor = list->head;
	list->cursor_index = 0;

	if (list->size == 0) {
		LL_pushBack(list, data);
		isInserted = true;
	}

	while (!isInserted) {
		if (list->cursor->next_ptr == NULL) {
			LL_pushBack(list, data);
			isInserted = true;
		}
		else if (compar(data, (list->cursor)->data) < 0) {
			LL_insertAt(list, data, list->cursor_index);
			isInserted = true;
		}
		else {
			list->cursor = (list->cursor)->next_ptr;
			(list->cursor_index)++;
		}
	}
}

void LL_removeByLookup(LList* list, void* interest_data)
{
}

void LL_removeByPrevious(LList* list, void* interest_data)
{
}

void LL_removeByIndex(LList* list, void* interest_data)
{
}

/*
Prints contents of the list for debugging purposes.
Params:
	LList pointer, list: the list to print contents of 
Returns:
	None
IO:
	Terminal: list contents
*/
void LL_printListPointers(LList* list)
{
	Node* cursor = list->head;
	
	if (list->head == NULL) {
		printf("List is empty\n");
	}
	else {
		do {
			printf("%p -> ", cursor);
			cursor = cursor->next_ptr;
		} while (cursor != NULL);
		printf("NULL\n");
	}

}


void LL_printListContents(LList *list, const char *(*toString)(const void *data))
{
	Node* cursor = list->head;

	if (list->head == NULL) {
		printf("List is empty\n");
	}
	else {
		do {
			printf("%s\n", toString(cursor->data));
			cursor = cursor->next_ptr;
		} while (cursor != NULL);
	}
}


void* LL_getData(LList* list, int index)
{
	Node* node = LL_getNode(list, index);
	
	if (node == NULL) {
		return NULL;
	}
	else {
		return node->data;
	}
}

Node* LL_getNode(LList* list, int index)
{
	bool move_up = true; //false to move down

	if ((index >= list->size) || (index < 0)) {
		printf("INDEX OUT OF BOUNDS\n");
		return NULL;
	}

	//determine where to set the cursor and which direction to move
	int c_to_i = index - list->cursor_index;	//dist from cursor to index (sign dictates direction)
	int t_to_i = list->size - index;			//dist from tail to index (size - index so it is always positive)
												//index represents dist from head
	
	if ((index <= abs(c_to_i)) && (index < t_to_i)) {			//index is closest to head
		list->cursor = list->head;
		list->cursor_index = 0;
		move_up = true;
	}
	else if ((abs(c_to_i) < index) && (abs(c_to_i) < t_to_i)) {	//index is closest to cursor
		if (c_to_i >= 0) {
			move_up = true;
		}
		else {
			move_up = false;
		}
	}
	else {		//index is closest to tail
		list->cursor = list->tail;
		list->cursor_index = list->size - 1;
		move_up = false;
	}
	
	if (move_up) {
		while (list->cursor_index < index) {
			//printf("moving up... %d\n", list->cursor_index);
			if ((list->cursor)->next_ptr != NULL) {
				list->cursor = (list->cursor)->next_ptr;
				(list->cursor_index)++;
			}
		}
	}
	else {
		while (list->cursor_index > index) {
			//printf("moving down... %d\n", list->cursor_index);
			if ((list->cursor)->prev_ptr != NULL) {
				list->cursor = (list->cursor)->prev_ptr;
				(list->cursor_index)--;
			}
		}
	}

	return list->cursor;
}

void LL_setData(LList* list, void* data, int index)
{
	Node* node_to_set = LL_getNode(list, index);
	node_to_set->data = data;
}
