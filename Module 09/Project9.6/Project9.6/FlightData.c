/*
Paul Wood

*/

#include "FlightData.h"
#include <stdlib.h>
#include <time.h>

void printFlightData(FlightData* fd) 
{
	char timestamp_readable[TIME_SIZE] = { NULL };
	
	time_t posix = fd->timestamp;
	struct tm _tm;
	gmtime_s(&_tm, &posix);
	asctime_s(timestamp_readable, TIME_SIZE, &_tm);

	printf("%s\t%s\t%s\t%s\n", fd->flight_num, fd->origin_code, fd->dest_code, timestamp_readable);
}

int compare_DESTCODE(const void *ptr1, const void *ptr2)
{
	return strcmp(((FlightData*)(ptr1))->dest_code, ((FlightData*)(ptr2))->dest_code);
}








