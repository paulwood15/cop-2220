#include <stdlib.h>
#include <stdio.h>
#include <math.h> 

int main(void) {
	int sizeInt = sizeof(int) * 8;
	int sizeFloat = sizeof(float) * 8;
	int sizeDouble = sizeof(double) * 8;
	int sizeLong = sizeof(long) * 8;
	int sizeLongLong = sizeof(long long) * 8;
	int sizeUnsigned = sizeof(unsigned) * 8;
	int sizeChar = sizeof(char) * 8;

	static int statDec = 42;

	unsigned long long valRangePos = 0;
	unsigned long long valRangeNeg = 0;

	printf("These are the sizes of the following data types:\n");
	printf("int: \t\t%d bits\n", sizeInt);
	printf("float: \t\t%d bits\n", sizeFloat);
	printf("double: \t%d bits\n", sizeDouble);
	printf("long: \t\t%d bits\n", sizeLong);
	printf("long long: \t%d bits\n", sizeLongLong);
	printf("unsigned: \t%d bits\n", sizeUnsigned);
	printf("char: \t\t%d bits\n\n", sizeChar);
	/*
	printf("static int: \t%d bits\n", sizeof(statDec));
	if (sizeof(statDec) == sizeof(int)) {
		printf("\tStatic variables doesnt change the size of the variable\n\n");
	}
	else {
		printf("\tStatic variables does change the size of the variable\n\n");
	}

	*/

	//int upper bound 
	for (int i = 0; i < 31; i++) {
		valRangePos += pow(2.0, i);
		//printf("%llu\n", valRangePos);
	}
	//int lower bound
	valRangeNeg = pow(2.0, sizeInt - 1);
	printf("The value range of an int is: -%llu to +%llu\n", valRangeNeg, valRangePos);
	valRangeNeg = 0;
	valRangePos = 0;

	//long upper bound 
	for (int i = 0; i < sizeLong - 1; i++) {
		valRangePos += pow(2.0, i);
		//printf("%llu\n", valRangePos);
	}
	//long lower bound
	valRangeNeg = pow(2.0, sizeLong - 1);
	printf("The value range of a long is: -%llu to +%llu\n", valRangeNeg, valRangePos);
	valRangeNeg = 0;
	valRangePos = 0;

	//long long upper bound 
	for (int i = 0; i < sizeLongLong - 1; i++) {
		valRangePos += pow(2.0, i);		//idk why it's giving me the same answer as the lower bond.
		//printf("%llu\n", valRangePos);
	}
	//long long lower bound
	valRangeNeg = pow(2.0, sizeLongLong - 1);
	printf("The value range of a long long is: -%llu to +%llu\n", valRangeNeg, valRangePos -1);
	valRangeNeg = 0;
	valRangePos = 0;

	//unsigned upper bound 
	for (int i = 0; i < sizeUnsigned - 1; i++) {
		valRangePos += pow(2.0, i);
		//printf("%llu\n", valRangePos);
	}
	//unsigned lower bound
	valRangeNeg = pow(2.0, sizeUnsigned - 1);
	printf("The value range of an unsigned is: -%llu to +%llu\n", valRangeNeg, valRangePos);
	valRangeNeg = 0;
	valRangePos = 0;

	//char upper bound 
	for (int i = 0; i < sizeChar - 1; i++) {
		valRangePos += pow(2.0, i);
		//printf("%llu\n", valRangePos);
	}
	//char lower bound
	valRangeNeg = pow(2.0, sizeChar - 1);
	printf("The value range of a char is: -%llu to +%llu\n", valRangeNeg, valRangePos);
	valRangeNeg = 0;
	valRangePos = 0;
}